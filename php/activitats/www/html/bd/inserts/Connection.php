<?php
function connect() {
  require_once 'dbconfig.php';

  $conn = new PDO("mysql:host={$dbconfig['server']};dbname={$dbconfig['db']};charset=utf8",
    $dbconfig['username'], $dbconfig['password']);
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  return $conn;
}

?>
