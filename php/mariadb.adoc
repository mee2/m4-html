= Ús bàsic de MariaDB
:doctype: article
:encoding: utf-8
:lang: ca
:numbered:
:ascii-ids:
:icons: font

== Usuaris

Quan arranca la màquina Vagrant hi ha dos usuaris creats al servidor MariaDB:

- `root`: és l'administrador del servidor. Pot connectar des de la pròpia
màquina o des de la màquina física (si tenim un client instal·lat). Utilitzarem
aquest usuari si hem de crear bases de dades noves, o modificar l'estructura de
bases de dades existents. Té contrasenya `super3`.
+
[NOTE]
====
Els usuaris del sistema Debian i del MariaDB són independents. Aquest usuari
_root_ no té res a veure amb l'usuari _root_ del sistema operatiu.
====

- `webuser`: és l'usuari que utilitzen els nostres programes PHP per
connectar-se. Té tots els permisos sobre la base de dades `hotel`. Té
contrasenya `super3`.

== Establir connexió

Per treballar amb el servidor MariaDB ho farem des de la pròpia màquina
virtual. Per connectar-nos a la màquina Vagrant ho fem amb `vagrant ssh`.

Un cop dins, per establir una connexió utilitzant l'usuari `root` de MariaDB,
farem:

[source,bash]
----
$ mysql -u root -p
----

De la mateixa manera, per establir una conneció amb l'usuari `webuser`:

[source,bash]
----
$ mysql -u webuser -p
----

L'opció *-u* permet indicar amb quin usuari connectem, i l'opció *-p* indica
que utilitzarem una contrasenya per autenticar-nos en el servidor.

== Ordres bàsiques

A continuació es presenten algunes ordres bàsiques per moure-ns en el sistema.

- `show databases;`: mostra una llista amb totes les bases de dades disponibles
al servidor. Com a mínim trobarem les bases de dades _mysql_ (on es guarda la
informació de tots els usuaris i permisos del servidor), _information_schema_
i _performance_schema_.

- `use <base de dades>;`: passem a utilitzar una de les bases de dades que hi
ha en el SGBD.

- `show tables;`: un cop estem utilitzant una base de dades concreta, amb
aquesta ordre podrem veure totes les taules que hi ha en aquesta BD.

- `desc <taula>;`: amb aquesta ordre podem veure l'estructura d'una de les
taules: quines columnes té i de quin tipus són, entre d'altres propietats.

- `select * from <taula>;`: podem executar qualsevol sentència SQL.

- `exit`: amb _exit_ o amb Ctrl+D tanquem la connexió al servidor.

Cal tenir en compte que la major part d'ordres han d'acabar en punt i coma. Si
no es posa, el sistema pensarà que encara no hem escrit l'ordre sencera, ja que
és habitual que les ordres SQL ocupin més d'una línia. Llavors, en tenim prou
amb escriure el punt i coma que ens hem deixat a la línia següent per poder
continuar:

[source,sql]
----
MariaDB [mysql]> select * from user
    -> ;
----
